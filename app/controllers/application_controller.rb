# -*- coding: utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :require_login

  private

  # ログインしていない場合ログイン画面へリダイレクト
  def require_login
    unless logged_in?
      flash[:error] = "You must be logged in to access this section"
      redirect_to new_login_url
    end
  end

  # ログインしているか？
  # @return {Boolean} ログインしている場合true
  def logged_in?
    # `!!`でtrue / falseに確実に変換
    !!current_user
  end

  # ログインしているユーザを返却
  # @return {User} ログインしている場合ユーザを返却、ログインしてない場合nil
  def current_user
    @_current_user ||= session[:user_id] and User.find(session[:user_id])
  end
end
