# -*- coding: utf-8 -*-
class LoginController < ApplicationController
  skip_before_filter :require_login, :only => [:new, :create]

  # ログイン画面
  def new
    logger.info "test!!!!!!"
  end

  # ログイン
  def create
    if user = User.authenticate(params[:name], params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Welcome!"
    end
  end

  # ログアウト
  def destroy
    @_current_user = session[:user_id] = nil
    redirect_to root_url
  end
end
