# -*- coding: utf-8 -*-
class User
  include Mongoid::Document
  include Mongoid::Encryptor

  field :name, :type => String
  field :password, :type => String
  encrypts :password

  validates_presence_of :name, :password

  # 認証する
  # @param {String} name 名前
  # @param {String} password パスワード
  def self.authenticate name, password
    user = self.where(name: name).first
    if user && user.password == password
      user
    else
      nil
    end
  end
end
