# -*- encoding: utf-8 -*-
require File.expand_path('../lib/fluent_access_log/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Tajima Junpei"]
  gem.email         = ["p.baleine@gmail.com"]
  gem.description   = %q{fluent_access_log}
  gem.summary       = %q{fluent_access_log}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "fluent_access_log"
  gem.require_paths = ["lib"]
  gem.version       = Rack::FluentAccessLog::VERSION

  gem.add_dependency "fluent-logger"  
end
