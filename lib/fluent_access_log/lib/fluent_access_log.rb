#coding: utf-8
require "fluent_access_log/version"
require 'rack'
require 'fluent-logger'

module Rack
  class FluentAccessLog
    def initialize(app, opts = {})
      @app = app
      @excludes = opts[:excludes] ? opts[:excludes] : []
      @mask = opts[:mask] ? opts[:mask] : 'xxx'
      @tag = opts[:tag] ? opts[:tag] : 'accesslog'
      host = opts[:host] ? opts[:host] : 'localhost'
      port = opts[:port] ? opts[:port].to_i : 24224
      Fluent::Logger::FluentLogger.open(nil, host: host, port: port)
    end

    def call(env)
      req = Rack::Request.new(env)
      p = req.params()
      @excludes.each do |it|
        if p[it]
          p[it] = @mask
        end
      end
      Fluent::Logger.post(@tag, {
        i: req.ip(),
        u: req.path(),
        a: req.user_agent(),
        p: p,
      })
      res = @app.call(env)
      res
    end
  end
end
