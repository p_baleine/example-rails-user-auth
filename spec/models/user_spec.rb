# -*- coding: utf-8 -*-
require 'spec_helper'

describe User do
  context "nameとpasswordを指定した場合" do
    before do
      @user = User.new(
        name: "山田太郎",
        password: "yamada"
      )
    end
    it "nameがセットされていること" do
      @user.name.should eq("山田太郎")
    end
  end

  context "nameが指定されていない場合" do
    before do
      @user = User.new password: "yamada"
    end
    it { @user.should_not be_valid }
  end

  context "passwordが指定されていない場合" do
    before do
      @user = User.new name: "山田太郎"
    end
    it { @user.should_not be_valid }
  end

  describe "認証周り" do
    before do
      @user = User.new(
        name: "山田太郎",
        password: "yamada"
      )
    end
    context "nameとpasswordが一致する場合" do
      it "認証成功すること" do
        pending
      end
    end
    context "nameとpasswordが一致しない場合" do
      it "認証失敗すること" do
        pending
      end
    end
  end
end

